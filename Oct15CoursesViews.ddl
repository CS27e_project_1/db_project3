drop view Oct15_person_view ;
drop view Oct15_teacher_view ;
drop view Oct15_student_view ;

create view Oct15_person_view as
SELECT 
    person_id,
    type,
    name,
    age
FROM Oct15_PERSON where type = 'oct15_person' ;

create or replace TRIGGER Oct15_person_trigger
     INSTEAD OF insert ON Oct15_person_view
     FOR EACH ROW
BEGIN
    insert into Oct15_person(
        person_id,
        type,
        name,
        age)
    VALUES ( 
        :NEW.person_id,
        'oct15_person',
        :NEW.name,
        :NEW.age) ;
END;
/

create view Oct15_teacher_view as
SELECT 
    person_id,
    type,
    age, office_id, email
FROM Oct15_person where type = 'oct15_teacher' ;

create or replace TRIGGER Oct15_teacher_trigger
     INSTEAD OF insert ON Oct15_teacher_view
     FOR EACH ROW
BEGIN
    insert into Oct15_person(
        person_id,
        type,
        age, email, office_id)
    VALUES ( 
        :NEW.person_id,
        'oct15_teacher',
        :NEW.age,
        :NEW.email,
        :NEW.office_id) ;
END;
/

create view Oct15_student_view as
SELECT 
    person_id,
    type,
    age, grade_id
FROM Oct15_person where type = 'oct15_student' ;

create or replace TRIGGER Oct15_student_trigger
     INSTEAD OF insert ON Oct15_student_view
     FOR EACH ROW
BEGIN
    insert into Oct15_person(
        person_id,
        type,
        age, grade_id)
    VALUES ( 
        :NEW.person_id,
        'oct15_student',
        :NEW.age,
        :NEW.grade_id) ;
END;
/